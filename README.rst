Code source du manuel QGIS BDD CAT ZH
=====================================

Ce dépôt contient le code source du manuel d'utilisation de QGIS pour les bases
de données des cellules d'assistance technique aux zones humides.

La compilation des différents formats de sortie (PDF, HTML, EPUB) se fait grâce
à `Sphinx`_.

.. _Sphinx: https://www.sphinx-doc.org/
